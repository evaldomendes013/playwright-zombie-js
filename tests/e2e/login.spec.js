const {test, expect} = require("@playwright/test");
const {LoginPage} = require("../pages/LoginPage");

let loginPage
test.beforeEach(async ({page}) => {
    loginPage = new LoginPage(page)

})
test("Deve logar como administrador", async ({page}) => {
    await loginPage.visit()
    await loginPage.submit('Evaldo Mendes', 'evaldo.mendes@xpto.com')
})