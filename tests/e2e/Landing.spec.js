const {test, expect} = require('@playwright/test');
const {LandingPage} = require('../pages/LandingPage')

let landingPage
test.beforeEach(async ({page}) => {
    landingPage = new LandingPage(page)

})
test('Deve cadastrar um lead na fila de espera', async ({page}) => {
    await landingPage.visit()
    await landingPage.openLeadModal()
    await landingPage.submitLeadForm('Evaldo Mendes', 'evaldo.mendes@xpto.com')
    const message = 'Agradecemos por compartilhar seus dados conosco. Em breve, nossa equipe entrará em contato!'
    await landingPage.toastHeaveText(message)
});
